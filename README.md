# esportacus-vue

[![Netlify Status](https://api.netlify.com/api/v1/badges/e455131c-4247-438c-b7d2-fe18a8503f9d/deploy-status)](https://app.netlify.com/sites/esportacus/deploys)

An app built with Emotion.sh and React for tracking your favorite E-Sports games, teams, and players!
